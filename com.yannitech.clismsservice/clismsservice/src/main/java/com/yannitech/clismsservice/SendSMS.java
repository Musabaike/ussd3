package com.yannitech.clismsservice;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;

public class SendSMS {

	public SendSMS()
    {
        super();
    }
	
	
    public static void main(String[] args) throws Exception
    {
    	String clipin = "01TEST111";
    	CamelContext context = new DefaultCamelContext();
    	SendSMS sendSms = new SendSMS();
    	String testEndpoint = "smpp://YanniBulkM@10.150.240.54:15019?password=Yannit1&sourceAddr=0811608594&enquireLinkTimer=3000&transactionTimer=5000&systemType=producer&sourceAddrNpi=1&sourceAddrTon=1&typeOfNumber=1";
    	context.start();
    	System.out.println(testEndpoint);
    	String message = String.format("Dear Customer! Your port-in validation PIN is %s and expires in 15 minutes. Please contact Telkom Mobile on 081 180 for more details", clipin);
    	sendSms.sendSmsMessage("0833140181", message, context, testEndpoint);
    	sendSms.sendSmsMessage("0767494491", message, context, testEndpoint);
    	sendSms.sendSmsMessage("0813888153", message, context, testEndpoint);
    	sendSms.sendSmsMessage("0652802050", message, context, testEndpoint);

		System.out.println(message);
		context.stop();
    }

	public void sendSmsMessage(String msisdn, String message, CamelContext ctx, String endpoint) throws Exception {

		Exchange exchange = new DefaultExchange(ctx);		
			
		Message msg = exchange.getIn();		
		msg.setBody(message);	 

		ProducerTemplate producer = ctx.createProducerTemplate();
		Integer i = 1 ;
		byte b = i.byteValue();
		
		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("CamelSmppDestAddr", msisdn.startsWith("0") ? msisdn.replaceFirst("0", "27") : msisdn);
		
		headers.put("CamelSmppSourceAddrTon", b);
		headers.put("CamelSmppDestAddrTon", b);
		
		msg.setHeaders(headers);
		exchange.setOut(msg);
		System.out.println("Sending SMS Message (MSISDN): " + msisdn);
		System.out.println("Sending SMS Header (Body): " + exchange.getOut().getHeaders().toString());
		System.out.println("Sending SMS Message (Body): " + exchange.getOut().getBody());	

		Exchange ex = producer.send(endpoint, exchange);
		
       	if(ex.getException() == null) {
       		System.out.println("** Message send messageID[{}]  **"+ ex.getOut().getHeader("CamelSmppId"));
       	}	else {
       		System.out.println("Could not send message "+ ex.getException());
       		throw ex.getException();
       	}
	}
	
}
