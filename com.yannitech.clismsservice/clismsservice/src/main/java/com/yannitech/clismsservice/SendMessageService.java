package com.yannitech.clismsservice;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendMessageService{
	
	static final Logger log = LoggerFactory.getLogger(SendMessageService.class);
	
	public void sendSmsMessage(String msisdn, String message, CamelContext ctx, String endpoint) throws Exception {

		Exchange exchange = new DefaultExchange(ctx);		
			
		Message msg = exchange.getIn();		
		msg.setBody(message);	 

		ProducerTemplate producer = ctx.createProducerTemplate();
		Integer i = 1 ;
		byte b = i.byteValue();
		
		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("CamelSmppDestAddr", msisdn.startsWith("0") ? msisdn.replaceFirst("0", "27") : msisdn);
		
		headers.put("CamelSmppSourceAddrTon", b);
		headers.put("CamelSmppDestAddrTon", b);
		
		msg.setHeaders(headers);
		exchange.setOut(msg);
		log.info("Sending SMS Message (MSISDN): " + msisdn);
		log.info("Sending SMS Header (Body): " + exchange.getOut().getHeaders().toString());
		log.info("Sending SMS Message (Body): " + exchange.getOut().getBody());	

		Exchange ex = producer.send(endpoint, exchange);
		
       	if(ex.getException() == null) {
       		log.info("** Message send messageID[{}]  **", ex.getOut().getHeader("CamelSmppId"));
       	}	else {
       		log.error("Could not send message", ex.getException());
       		throw ex.getException();
       	}
	}
}

