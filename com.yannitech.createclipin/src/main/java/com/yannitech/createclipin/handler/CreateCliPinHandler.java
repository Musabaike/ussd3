package com.yannitech.createclipin.handler;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.ws.addressing.AttributedURIType;

import za.co.telkom.eai.enterpriseservicemetadata._20110801.SenderIDType;

public class CreateCliPinHandler extends AbstractSoapInterceptor {

	private final static QName _FROM_QNAME = new QName("http://www.w3.org/2005/08/addressing", "From");
	private final static QName _ACTION_QNAME = new QName("http://www.w3.org/2005/08/addressing", "Action");
	private final static QName _MessageID_QNAME = new QName("http://www.w3.org/2005/08/addressing", "MessageID");
	private final static String CREATE_STATUS_NAME = "CreatePortInVerificationPin";
	
	public CreateCliPinHandler() {
		super(Phase.PRE_LOGICAL);
	}
	
	public void handleMessage(SoapMessage message) throws Fault {
		
		SenderIDType sedId = new za.co.telkom.eai.enterpriseservicemetadata._20110801.ObjectFactory().createSenderIDType();
		sedId.setEndUserIdentity("?");
		sedId.setValue("Ussd gateway");
		
		za.co.telkom.eai.enterpriseservicemetadata._20110801.MetadataType.Param param = new za.co.telkom.eai.enterpriseservicemetadata._20110801.MetadataType.Param();		
		param.setKey("CALL_STACK");
		param.setValue(CREATE_STATUS_NAME);
		param.setQualifier("EAI-CORE");
		
		za.co.telkom.eai.enterpriseservicemetadata._20110801.Metadata data = new za.co.telkom.eai.enterpriseservicemetadata._20110801.Metadata();
		data.getParam().add(param);
		
		org.apache.cxf.ws.addressing.From.ReferenceParameters ref  = new org.apache.cxf.ws.addressing.From.ReferenceParameters();		
		ref.setSenderID(sedId);
		ref.setMetadata(data);
		
		org.apache.cxf.ws.addressing.From from = new org.apache.cxf.ws.addressing.From();		
		from.setReferenceParameters(ref);
		
		JAXBElement<org.apache.cxf.ws.addressing.From> head1 =new JAXBElement<org.apache.cxf.ws.addressing.From>(_FROM_QNAME, org.apache.cxf.ws.addressing.From.class, null, from);
		
		AttributedURIType attUr = new AttributedURIType();
		attUr.setValue(CREATE_STATUS_NAME);
		
		JAXBElement<AttributedURIType> head2 = new JAXBElement<AttributedURIType>(_ACTION_QNAME, AttributedURIType.class, null, attUr);
		
		AttributedURIType attUr3 = new AttributedURIType();
		attUr3.setValue("");
		JAXBElement<AttributedURIType> head3 = new JAXBElement<AttributedURIType>(_MessageID_QNAME, AttributedURIType.class, null, attUr3);
		
		try {
			
			message.getHeaders().add(new Header(head1.getName(), from, new JAXBDataBinding(org.apache.cxf.ws.addressing.From.class)));
			message.getHeaders().add(new Header(head2.getName(), attUr, new JAXBDataBinding(AttributedURIType.class)));
			message.getHeaders().add(new Header(head3.getName(), attUr3, new JAXBDataBinding(AttributedURIType.class)));
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}