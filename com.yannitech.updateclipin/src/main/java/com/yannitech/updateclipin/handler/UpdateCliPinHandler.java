package com.yannitech.updateclipin.handler;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.ws.addressing.AttributedURIType;
import org.apache.cxf.ws.addressing.From;
import org.apache.cxf.ws.addressing.From.ReferenceParameters;

import za.co.telkom.eai.enterpriseservicemetadata._20110801.Metadata;
import za.co.telkom.eai.enterpriseservicemetadata._20110801.SenderIDType;

import java.security.SecureRandom;

public class UpdateCliPinHandler extends AbstractSoapInterceptor {

	private final static QName _FROM_QNAME = new QName("http://www.w3.org/2005/08/addressing", "From");
	private final static QName _ACTION_QNAME = new QName("http://www.w3.org/2005/08/addressing", "Action");
	private final static QName _MessageID_QNAME = new QName("http://www.w3.org/2005/08/addressing", "MessageID");
	private final static String UPDATE_STATUS_NAME = "http://eai.telkom.co.za/services/UpdatePortInVerificationStatus/20150105/Request";
	private final static String ADDRESS = "http://www.w3.org/2005/08/addressing/anonymous";
	
	public UpdateCliPinHandler() {
		super(Phase.PRE_LOGICAL);
	}
	
	public void handleMessage(SoapMessage message) throws Fault {
		
		SenderIDType sedId = new SenderIDType();
		sedId.setEndUserIdentity("?");
		sedId.setValue("USSD");
		
		Metadata data = new Metadata();
		
		ReferenceParameters ref  = new ReferenceParameters();		
		ref.setSenderID(sedId);
		ref.setMetadata(data);
		
		AttributedURIType attUr4 = new AttributedURIType();
		attUr4.setValue(ADDRESS);
		
		From from = new From();		
		from.setAddress(attUr4);
		from.setReferenceParameters(ref);
		
		JAXBElement<From> head1 =new JAXBElement<From>(_FROM_QNAME, From.class, null, from);
		
		AttributedURIType attUr = new AttributedURIType();
		attUr.setValue(UPDATE_STATUS_NAME);
		
		JAXBElement<AttributedURIType> head2 = new JAXBElement<AttributedURIType>(_ACTION_QNAME, AttributedURIType.class, null, attUr);
		
		AttributedURIType attUr3 = new AttributedURIType();
		attUr3.setValue(generateRandomNumber());
		JAXBElement<AttributedURIType> head3 = new JAXBElement<AttributedURIType>(_MessageID_QNAME, AttributedURIType.class, null, attUr3);
		
		try {
			
			message.getHeaders().add(new Header(head1.getName(), from, new JAXBDataBinding(From.class)));
			message.getHeaders().add(new Header(head2.getName(), attUr, new JAXBDataBinding(AttributedURIType.class)));
			message.getHeaders().add(new Header(head3.getName(), attUr3, new JAXBDataBinding(AttributedURIType.class)));
			
		} catch (JAXBException e) {
			
			e.printStackTrace();
		}
		
	
	}
	
	public String generateRandomNumber() {
		
		SecureRandom rand = new SecureRandom(); 
	    int rand_int1 = rand.nextInt(10); 
	    int rand_int2 = rand.nextInt(10); 
	    int rand_int3 = rand.nextInt(10); 
	    int rand_int4 = rand.nextInt(10); 
	    int rand_int5 = rand.nextInt(10); 
	    int rand_int6 = rand.nextInt(10); 
	  
	    return rand_int1 +""+ rand_int2 + rand_int3 + rand_int4 + rand_int5 + rand_int6;
	}


}

